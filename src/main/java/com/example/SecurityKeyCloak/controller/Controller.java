package com.example.SecurityKeyCloak.controller;

import com.example.SecurityKeyCloak.feign.RemoteServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class Controller {

    @Autowired
    RemoteServiceClient remoteServiceClient;

    @GetMapping("/hello")
    public ResponseEntity<?> helloRequest(){
        return ResponseEntity.ok("Welcome!");
    }

    @GetMapping("/clubs")
    public ResponseEntity<?> allClubs(){
        return ResponseEntity.ok(remoteServiceClient.getAllClubs());
    }
    @GetMapping("/players")
    public ResponseEntity<?> allPlayers(){
        return ResponseEntity.ok(remoteServiceClient.getAllPlayers());
    }
}

package com.example.SecurityKeyCloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SecurityKeyCloakApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityKeyCloakApplication.class, args);
	}

}

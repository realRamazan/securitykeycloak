package com.example.SecurityKeyCloak.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "transferwindow", url = "http://localhost:8082/")
public interface RemoteServiceClient {
    @GetMapping("/clubs")
    public ResponseEntity<?> getAllClubs();

    @GetMapping("/players")
    public ResponseEntity<?> getAllPlayers();
}
